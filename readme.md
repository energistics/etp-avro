#etp-avro

This is an Apache Avro serialization library for the Energistics Transfer protocol (ETP).

ETP is a proposed specification for streaming real-time data from oil field drilling and production facilities. It uses websockets for transport and Apache Avro for serialization. This node package 
contains:

1. A set of routines for serializing and deserialing binary Avro messages in javascript.

#Prerequisites
- Install Node from nodejs.org - v0.10 min required.

#Installation

## To install from npm
	npm install etp-avro

#Using the library.
### Create test.js
    var avro = require("etp-avro");
	var schemaCache = new avro.SchemaCache( [] );
	var reader = new avro.BinaryReader(schemaCache);
	
	var msg = reader.decode("string", [22, 0x48, 0x65, 0x6c, 0x6c, 0x6f, 0x20, 0x57, 0x6f, 0x72, 0x6c, 0x64]);

	console.log(msg);

Run it in node

	$node ./test.js
	Hello World
	
	$
