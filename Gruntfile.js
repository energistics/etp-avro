module.exports = function(grunt) {

    grunt.initConfig({
        ts: {
            default : {
                tsconfig: "."
            }
        },
        browserify: {
            dist: {
                files: {
                    "./lib/browser/etp-avro.js": ["./lib/etp-avro.js"]
                }
            }
        },
		mochaTest: {
			default: {
				options: {
					
				},
				src: ['test/*.js']
			},
			xunit: {
				options: {
				  reporter: 'xunit',
				  captureFile: 'temp/xunit.xml'
				},
				src: ['test/*.js']
			  }
		},
        typings: {
            install: {}
        },
        uglify: {
            options: {
                // the banner is inserted at the top of the output
                banner: '/*! bootstrap <%= grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            dist: {
                files: {
                    './lib/browser/etp-avro.min.js' : ['./lib/browser/etp-avro.js']
                }
            }
        },
        watch: {
        }

    });


    grunt.loadNpmTasks("grunt-browserify")
    grunt.loadNpmTasks("grunt-contrib-uglify")
    grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-mocha-test');
    grunt.loadNpmTasks("grunt-ts");
    grunt.loadNpmTasks('grunt-typings');

    grunt.registerTask('init', ['typings']);
    grunt.registerTask('build', ['ts', 'browserify', 'uglify']);
	grunt.registerTask('test', ['mochaTest:default']);
	// xunit output for CI
	grunt.registerTask('xunit', ['mochaTest:xunit']);
	grunt.registerTask('all', ['init', 'build', 'test']);
	
    grunt.registerTask('default', ['build']);
};