/*global it, describe */
var assert = require("assert"),
    should = require("should"),
    Avro = require("../lib/etp-avro"),
    reader = new Avro.BinaryReader(),
    writer = new Avro.BinaryWriter(),
	enumSchema = {type: "enum", name: "directions", symbols:["Up", "Down"]},
    nullableString = ['null', 'string'],
    nullableInt = ['null', 'int'],
    nullableDouble = ['null', 'double'],
    nullableMany = ['null', 'string', 'int', 'double'],
    dateTimeschema = {"type": "record","namespace": "Energistics.Datatypes","name": "DateTime","fields":[{ "name": "time", "type": "long" },{ "name": "offset", "type": "int" }]},
    nullableStruct = ['null', dateTimeschema];
    tests = [
        /* Avro binary */                   /* Js */        /* Schema */
        [[1],                               -1,             'int'],
        [[2],                               1,              'int'],
        [[3],                               -2,             'int'],
        [[4],                               2,              'int'],
        [[0],                               0,              'int'],
        [[1],                               -1,             'long'],
        [[0x13],                            -10,            'long'],
        [[0xc7, 0x1],                       -100,           'long'],
        [[0xcf, 0x0f],                      -1000,          'long'],
        [[0x9f, 0x9c, 0x01],                -10000,         'long'],
        [[0xbf, 0x9a, 0x0c],                -100000,        'long'],
        [[0xff, 0x88, 0x7a],                -1000000,       'long'],
        [[0xff, 0xd9, 0xc4, 0x09],          -10000000,      'long'],
        [[0xff, 0x83, 0xaf, 0x5f],          -100000000,     'long'],
        [[0xff, 0xa7, 0xd6, 0xb9, 0x07],    -1000000000,    'long'],
        [[0xff, 0x8f, 0xdf, 0xc0, 0x4a],    -10000000000,   'long'],
        [[2],                               1,              'long'],
        [[0x14],                            10,             'long'],
        [[0xc8, 0x1],                       100,            'long'],
        [[0xd0, 0x0f],                      1000,           'long'],
        [[0xa0, 0x9c, 0x01],                10000,          'long'],
        [[0xc0, 0x9a, 0x0c],                100000,         'long'],
        [[0x80, 0x89, 0x7a],                1000000,        'long'],
        [[0x80, 0xda, 0xc4, 0x09],          10000000,       'long'],
        [[0x80, 0x84, 0xaf, 0x5f],          100000000,      'long'],
        [[0x80, 0xa8, 0xd6, 0xb9, 0x07],    1000000000,     'long'],
        [[0x80, 0x90, 0xdf, 0xc0, 0x4a],    10000000000,    'long'],

        // 2^53, The largest effective integer value that we can put in a double
        // without loss of precision. Negative is 1 power of 2 smaller.
        [[128, 128, 128, 128, 128, 128, 128, 32], 9007199254740992, 'long'],
        [[255, 255, 255, 255, 255, 255, 255, 15], -4503599627370496, 'long'],

        [[0],                                       0,          'long'],
        [[1],                                       true,       'boolean'],
        [[0],                                       false,      'boolean'],
        [[6, 65, 66, 67],                           "ABC",      'string'],
        [[8, 65, 66, 0xc3, 0x87],                   "ABÇ",      'string'],
        [[12, 0x41, 0x42, 0xf0, 0x9d, 0x9c, 0x8d],  "AB𝜍",      'string'],
        [[4, 0xc3, 0xbf],                           "\u00ff",   "string"],
        [[0, 0, 0, 0, 0, 0, 0, 0 ],                 0,          'double'],
        [[0, 0, 0, 0 ],                             0,          'float'],
        [[0, 0, 0, 0, 0, 0, 240, 63 ],              1,          'double'],
        [[0, 0, 128, 63 ],                          1,          'float'],
        [[0, 0, 0, 0, 0, 0, 240, 191 ],             -1,         'double'],
        [[0, 0, 128, 191],                          -1,         'float'],
        [[0, 0, 0, 0, 0, 192, 94, 64],              123,        'double'],
        [[0, 0, 246, 66],                           123,        'float'],
        [[174, 71, 225, 122, 20, 174, 243, 63 ],    1.23,       'double'],
        [[164, 112, 157, 63 ],                      1.2300000190734863,       'float'],
        [[138, 73, 108, 59, 130, 117, 176, 84 ],    9e+99,      'double'],
        [[0, 2],                         {int:1},    ['int', 'string']]
    ];
	
var schemaCache = new Avro.SchemaCache([JSON.parse(JSON.stringify(dateTimeschema))]);

function testOneDecoding(test) {
    it('should decode [' + test[0] + '] as ' + test[1], function () {
        assert.deepEqual(test[1], reader.decode(test[2], test[0]));
    });
}

function testOneEncoding(test, splain) {
    it('should encode ' + test[1] + ' as [' + test[0] + '] ' + (splain?splain:""), function () {
        var result = writer.encode(test[2], test[1]),
            actual = [],
            i;
        for (i = 0; i < result.length; i++) {
            actual[i] = result[i];
        }
        assert.deepEqual(test[0], actual);
        assert.equal(writer.getBuffer().length, actual.length);
    });
}

describe('BinaryReader', function () {
    describe('#decode()', function () {
        var i;
        for (i = 0; i < tests.length; i++) {
            testOneDecoding(tests[i]);
        }
    });
    it("should decode bytes as Uint8Array", function () {
        var retVal = reader.decode("bytes", [6, 45,0x66,0x24]);
        assert.equal(retVal.length, 3);
        assert.equal(retVal[0], 45);
        assert.equal(retVal[1], 0x66);
        assert.equal(retVal[2], 0x24);
        assert.equal(retVal.BYTES_PER_ELEMENT, 1);
    });
    it("should decode double NaN correctly", function () {
        assert.equal(isNaN(reader.decode("double", [0, 0, 0, 0, 0, 0, 248, 127 ])), true);
    });
    it("should decode float NaN correctly", function () {
        assert.equal(isNaN(reader.decode("float", [0, 0, 192, 127 ])), true);
    });
    testOneDecoding([[0], null, nullableString], "when datum is a union of null and string");
    testOneDecoding([[2, 6, 65, 66, 67], "ABC", nullableString], "when datum is a union of null and string");

    testOneDecoding([[0], null, nullableInt], "when datum is a union of null and int");
    testOneDecoding([[2, 0xbf, 0x9a, 0x0c], -100000, nullableInt], "when datum is a union of null and int");

    testOneDecoding([[0], null, nullableDouble], "when datum is a union of null and double");
    testOneDecoding([[2, 174, 71, 225, 122, 20, 174, 243, 63 ], 1.23,  nullableDouble], "when datum is a union of null and double");

    testOneDecoding([[6, 174, 71, 225, 122, 20, 174, 243, 63 ],  { "double": 1.23 }, nullableMany], "when datum is a union of null and double");
    testOneDecoding([[4, 0xbf, 0x9a, 0x0c], {"int": -100000}, nullableMany], "when datum is a union of null and int");
    testOneDecoding([[2, 6, 65, 66, 67], { "string": "ABC" }, nullableMany], "when datum is a union of null and string");

});

// Create an identity matrix of size n x n
function im(n) {
    return Array.apply(null, new Array(n)).map(function(x, i, a) { return a.map(function(y, k) { return i === k ? 1 : 0; }) });
}

describe('BinaryWriter', function(){
    describe("#getBuffer() containing ABC", function(){
        writer.encode('string', "ABC");
        var buf = writer.getBuffer();
        it("Should be length 4", function(){buf.length.should.equal(4)})
        it("Should should have string length 3", function(){buf[0].should.equal(6)})
        it("Should have A", function(){buf[1].should.equal(65)})
        it("Should have B", function(){buf[2].should.equal(66)})
        it("Should have C", function(){buf[3].should.equal(67)})
    })
})

describe('BinaryWriter', function () {
    describe('#encode()', function () {
        var i = 0;
        for (i = 0; i < tests.length; i++) {
            testOneEncoding(tests[i]);
        }
        testOneEncoding([[0], null, nullableString], "when datum is a union of null and string");
        testOneEncoding([[2, 6, 65, 66, 67], "ABC", nullableString], "when datum is a union of null and string");
        testOneEncoding([[2, 6, 65, 66, 67], { "string": "ABC" }, nullableString], "when datum is a union of null and string");

        testOneEncoding([[0], null, nullableInt], "when datum is a union of null and int");
        testOneEncoding([[2, 0xbf, 0x9a, 0x0c], -100000, nullableInt], "when datum is a union of null and int");
        testOneEncoding([[2, 0xbf, 0x9a, 0x0c], {"int": -100000}, nullableInt], "when datum is a union of null and int");

        testOneEncoding([[0], null, nullableInt], "when datum is a union of null and double");
        testOneEncoding([[2, 174, 71, 225, 122, 20, 174, 243, 63 ], 1.23,  nullableDouble], "when datum is a union of null and double");
        testOneEncoding([[2, 174, 71, 225, 122, 20, 174, 243, 63 ],  { "double": 1.23 }, nullableDouble], "when datum is a union of null and double");

        testOneEncoding([[0xc0, 0x9a, 0x0c, 2], {time: 100000, offset: 1}, dateTimeschema]);

        testOneEncoding([[0], null, nullableStruct], "when datum is a union of null and double");
        testOneEncoding([[2, 0xc0, 0x9a, 0x0c, 2], {time: 100000, offset: 1}, nullableStruct]);
        //testOneEncoding([[2, 0xc0, 0x9a, 0x0c, 2], { "DateTime": {time: 100000, offset: 1}}, nullableStruct]);

        it("should encode Uint8Array as bytes", function () {
            var retVal = writer.encode("bytes", new Uint8Array([45,0x66,0x24]));
            assert.equal(retVal.length, 4);
            assert.equal(retVal[0], 6);
            assert.equal(retVal[1], 45);
            assert.equal(retVal[2], 0x66);
            assert.equal(retVal[3], 0x24);
            assert.equal(retVal.BYTES_PER_ELEMENT, 1);
        });
        testOneEncoding([[0, 0, 0, 0, 0, 0, 248, 127 ], NaN, "double"], " when its a double");
        testOneEncoding([[0, 0, 192, 127 ], NaN, "float"], "when its a float");

        it("should encode large arrays correctly", function () {
            var data = im(1000); // identity matrix with 1000000 elements.
            writer.encode({"type": "array", "items": "int"}, im);
        });

        it("should encode enums from strings", function(){
			writer.encode(enumSchema, "Up");
			writer.encode(enumSchema, "Down");
		});
		it("should encode enums from ints", function(){
			writer.encode(enumSchema, 0);
			writer.encode(enumSchema, 1);
		});
		it("should not encode enums from invalid strings", function(){
			(function(){writer.encode(enumSchema, "Sideways")}).should.throw();
		});
		it("should case sensitive for enums", function(){
			(function(){writer.encode(enumSchema, "down")}).should.throw();
		});
		it("should not encode enums from ints out of range", function(){
			(function(){writer.encode(enumSchema, 4)}).should.throw();
		});
		it("should not encode enums from ints out of range", function(){
			(function(){writer.encode(enumSchema, -1)}).should.throw();
		});
		it("should not encode enums from ints out of range", function(){
			(function(){writer.encode(enumSchema, -1.87364543)}).should.throw();
		});
    });
});


describe('SchemaCache', function () {
    describe('#validate()', function () {
		it("Should validate true as boolean", function () { (function(){schemaCache.validate("boolean", true)}).should.not.throw() });
		it("Should validate false as boolean", function () { (function(){schemaCache.validate("boolean", false)}).should.not.throw() });
		it("Should validate {time: 12324234, offset: 234} as DateTime", function () { (function(){schemaCache.validate("Energistics.Datatypes.DateTime", {time: 12324234,offset: 12})}).should.not.throw() });
		it("Should not validate {time: 12324234} as DateTime", function () { (function(){schemaCache.validate("Energistics.Datatypes.DateTime", {time: 12324234})}).should.throw() });
    });
});

