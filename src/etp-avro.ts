///
 //License notice
 //  
 // Energistics copyright 2017-
 // Energistics Transfer Protocol
 //
 // All rights in the WITSML� Standard, the PRODML� Standard, and the RESQML� Standard, or
 // any portion thereof, shall remain with Energistics or its suppliers and shall remain 
 // subject to the terms of the Product License Agreement available at
 // http://www.energistics.org/product-license-agreement. 
 // 
 // Apache
 // Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 // except in compliance with the License. 
 // 
 // You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 // 
 // Unless required by applicable law or agreed to in writing, software distributed under the
 // License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 // either express or implied. 
 // 
 // See the License for the specific language governing permissions and limitations under the
 // License.
 // 
 // All rights reserved. 
 // 
 ///

declare  function require(path: string) : any;

var ty = require("./assert-type");
export var T = ty.Assert;


export class BinaryReader {
	constructor(schemas: any, buffer: Uint8Array) {
		this.schemas = schemas;
		this.buffer = buffer;
		if(this.buffer !== undefined)
			this.dataView = new DataView(this.buffer.buffer);
	}

	strictMode: boolean=false;
	buffer: Uint8Array = null;
	dataView: DataView;
	idx: number = 0;
	schemas: any = {};

	decode(schema, buffer: number[]) {
		// this.storeSchemas(schema);
		this.buffer = new Uint8Array(buffer);
		this.dataView = new DataView(this.buffer.buffer);
		this.idx = 0;
		return this.readDatum(schema);
	}

	readByte() {
		return this.buffer[this.idx++];
		//return this.buffer.charCodeAt(this._index++);
	}

	typeOf(value) {
		var s : any = typeof value;
		if (s === 'object') {
			if (value) {
				if (value instanceof Array) {
					s = 'array';
				}
			} else {
				s = 'null';
			}
		}
		return s;
	} // typeOf


	read32le() {
		var b;
		var v = 0;
		var i;
		for (i = 0; i < 32; i += 8) {
			b = this.readByte();
			v |= (b << i);
		}
		return v;
	}

	// Reads count for array and map
	readCount() {
		var count = this.readLong();
		if (count < 0) {
			this.readLong();
			count = -count;
		}
		return count;
	}

	readBoolean() {
		return (this.readByte() === 1);
	}

	readInt() {
		var i;
		var b = this.readByte();
		var n = b & 0x7f;

		for (i = 7; i <= 28 && b > 0x7f; i += 7) {
			b = this.readByte();
			n |= (b & 0x7f) << i;
		}

		if (b > 0x7f) {
			throw "Invalid int encoding.";
		}

		return (n >>> 1) ^ -(n & 1);
	}

	readLong() {
		// using integer math instead
		// of bitwise operations allows us to go
		// quite a bit higher in the value.
		var b = this.readByte();
		var n = b & 0x7F;
		//console.log(n);
		var negative = (b&0x01)==0x01;
		var shift = 128;

		while ((b & 0x80) !== 0) {
			b = this.readByte();
			//n |= (b & 0x7F) << shift

			n += (b & 0x7f) * shift;
			//console.log(((b & 0x7f)*shift) + ":" + shift);
			shift *= 128;
		}

		//if(n<0xffffffff)
		//    return (n >> 1) ^ -(n & 1)
		if(negative)
			return Math.ceil(n/2)*-1;
		return n / 2;
	}

	readFloat() : number {
		var result = this.dataView.getFloat32(this.idx, true);
		this.idx += 4;
		return result;
	}

	readDouble() : number {
		var result = this.dataView.getFloat64(this.idx, true);
		this.idx += 8;
		return result;
	}

	readFixed(len: number) {

		var result = [];
		var i;
		for (i = 0; i < len; i++) {
			result.push(this.readByte());
		}
		return result;
	}

	readBytes() {
		var length = this.readLong();
		var retVal = new Uint8Array(this.buffer.buffer, this.idx, length);
		this.idx += length;
		return retVal;
	}

	readString() {
		var buff = this.readBytes();
		return new Buffer(buff).toString('utf-8');
	}

	readEnum() {
		return this.readInt();
	}

	readArrayStart() {
		return this.readCount();
	}

	arrayNext() {
		return this.readCount();
	}

	readMapStart() {
		return this.readCount();
	}

	mapNext() {
		return this.readCount();
	}

	readArray(schema: any) {
		var result = [];
		var i = this.readArrayStart();
		while (i !== 0) {
			while (i-- > 0) {
				result.push(this.readDatum(schema.items));
			}
			i = this.arrayNext();
		}
		return result;
	}

	readDatum(schema: any): any {
		var type;
		var i;
		var result;
		type = this.typeOf(schema);

		switch (type) {
			case "object":
				type = schema.type;
				break;
			case "string":
				type = schema;
				break;
			case "array":
				type = "union";
				break;
			default:
				throw "R:Invalid schema type: " + type;
		}

		if (type == "null") {
		  return null;
		} else if (type == "boolean") {
			return this.readBoolean();
		} else if (type == "int") {
			return this.readInt();
		} else if (type == "long") {
			return this.readLong();
		} else if (type == "float") {
			return this.readFloat();
		} else if(type == "double") {
			return this.readDouble();
		} else if (type == "bytes") {
			return this.readBytes();
		} else if (type == "string") {
			return this.readString();
		} else if (type == "record") {
			result = {};
			for (i = 0; i < schema.fields.length; i++) {
				result[schema.fields[i].name] = this.readDatum(schema.fields[i].type);
			}
			return result;
		} else if (type == "enum") {
			return schema.symbols[this.readEnum()];
		} else if (type == "array") {
			return this.readArray(schema);
		} else if (type == "map") {
			result = {};
			i = this.readMapStart();
			while (i !== 0) {
				while (i-- > 0) {
					result[this.readDatum("string")] = this.readDatum(schema.values);
				}
				i = this.mapNext();
			}
			return result;
		} else if (type == "union") {
			var idx = this.readLong();
			var result;
			if (schema[idx] == "null") {
				result = null;
			}
            else if(schema.length==2 && schema[0]=='null' && idx==1) {
                result = this.readDatum(schema[1]);
            }
			else {
				result = {};
				result[schema[idx]] = this.readDatum(schema[idx]);
			}
			return result;
		} else {
			if (this.schemas[type] === undefined) {
				throw "Unsupported schema type " + type;
			}
			return this.readDatum(this.schemas[type]);
		}
	}
}

export class BinaryWriter {
	constructor(schemas) {
		this.dataView = new DataView(this.buffer.buffer);
		if (schemas !== undefined) {
			this.schemas = schemas;
		}
	}

	buffer: Uint8Array = new Uint8Array(2048);
	dataView: DataView;
	_index: number = 0;
	schemas = {};

	getBuffer() {
		var buffer = new Buffer(this._index);
		for (var i = 0; i < this._index; ++i) {
			buffer[i] = this.buffer[i];
		}
		return buffer;
	}

	getArrayBuffer() {
		var ab: any = this.buffer.buffer;
		return ab.slice(0, this._index);
	}

	alloc(size: number) {
		this.buffer = new Uint8Array(size);
		this.dataView = new DataView(this.buffer.buffer);
	}

	realloc(size: number) {
		var old = this.buffer;
		this.alloc(size * 1.6);
		this.buffer.set(old);
	}

	require(bytes) {
		if (this.buffer.length < this._index + bytes) {
			this.realloc(this._index + bytes);
		}
	}

	encode(schema, datum) {
		this._index = 0;
		this.writeDatum(schema, datum);
		return this.buffer.subarray(0, this._index);
	}

	writeByte(b) {
		this.require(1);
		this.buffer[this._index++] = b;
	}

	writeBoolean(value) {
		this.writeByte(value ? 1 : 0);
	}

	writeInt(value) {
		var n = ((value << 1) ^ (value >> 31));
		while ((n & ~0x7F) !== 0) {
			this.writeByte(((n & 0x7f) | 0x80));
			n >>= 7;
		}
		this.writeByte(n);
	}

	writeLong(value) {
		var n;
		if ((value & 0xffffffff) == value) {
			this.writeInt(value);
		} else {
			if (value > 0) {
				n = 2 * value;
			}
			else {
				n = Math.round((2 * Math.abs(value)) - 1);
			}
			while (n > 0x7F) {
				this.writeByte(((n & 0x7f) | 0x80));
				n /= 128;
			}
			this.writeByte(n&0x7f);
		}
	}

	writeFloat(f) {
		this.require(4);
		this.dataView.setFloat32(this._index, f, true);
		this._index += 4;
	}

	writeDouble(value) {
		this.require(8);
		this.dataView.setFloat64(this._index, value, true);
		this._index += 8;
	}

	writeBytes(bytes: Uint8Array) {
		this.writeLong(bytes.length);
		this.require(bytes.length);
		this.buffer.set(bytes, this._index);
		this._index += bytes.length;
	}

	writeString(str: string) {
		if(str==null || str.length==0) {
			this.writeInt(0);
			return;
		}
		// default encoding is utf-8
		var buff=new Buffer(str);
		this.writeBytes(new Uint8Array(buff));
	}

	writeIndex(idx) {
		this.writeInt(idx);
	}

	writeMapStart() {
		// To Be Implemented
	}

	writeMapEnd() {
		// To Be Implemented
	}

	writeDatum(schema: any, datum) {
		var type;
		var i;
		var result;

		function typeOf(value) {
			var s:any = typeof value;
			if (s === 'object') {
				if (value) {
					if (value instanceof Array) {
						s = 'array';
					}
				} else {
					s = 'null';
				}
			}
			return s;
		} // typeOf

		type = typeOf(schema);
		if (type === "object") {
			type = schema.type;
		}
		else if (type === "string") {
			type = schema;
		}
		else if (type === "array") {
			type = "union";
		}
		else if (type === "undefined") {
			throw "W:Undefined schema type " + schema;
		}
		else {
			throw "W:Unrecognized schema type: " + type + schema;
		}

		switch (type) {
			// Primitive types
			case "null":
				break;
			case "boolean":
				this.writeBoolean(datum);
				break;
			case "int":
				this.writeInt(datum);
				break;
			case "long":
				this.writeLong(datum);
				break;
			case "float":
				this.writeFloat(datum);
				break;
			case "double":
				this.writeDouble(datum);
				break;
			case "bytes":
				this.writeBytes(datum);
				return;				
			case "string":
				this.writeString(datum);
				return;
			// Complex types
			case "record":
				for (i = 0; i < schema.fields.length; i++) {
					this.writeDatum(schema.fields[i].type, datum[schema.fields[i].name]);
				}
				return;

			case "enum":
				for (i = 0; i < schema.symbols.length; i++) {
					if (schema.symbols[i] == datum) {
						this.writeInt(i);
						return;
					}
				}
				if ( (parseInt(datum)>=0) && (parseInt(datum)<schema.symbols.length) ) {
					this.writeInt(datum);
					return;
				}
				throw new Error("Invalid enum value: " + datum + " expecting: " + schema.symbols);

			case "array":
                // Friendly for javascript null array === zero-length array
				if (datum && datum.length > 0) {
					this.writeLong(datum.length);
					for (i = 0; i < datum.length; i++) {
						this.writeDatum(schema.items, datum[i]);
					}
				}
				this.writeLong(0);
				return;

			case "map":
				var count = 0;
				for (var thisVar in datum) {
					if (datum.hasOwnProperty(thisVar)) {
						++count;
					}
				}

				if (count > 0) {
					this.writeLong(count);
					for (var k in datum) {
						this.writeString(k);
						this.writeDatum(schema.values, datum[k]);
					}
				}
				this.writeLong(0);
				break;

			case "union":
                /// Special handling for nullable unions in ETP
                if(schema[0]=='null') {
                    if(datum==null) {
                        this.writeLong(0);
                        return;
                    }
                    else if ( (schema.length==2) && (!(datum.hasOwnProperty(schema[1]))) ) {
                        this.writeLong(1);
                        this.writeDatum(schema[1], datum);
                        return;
                    }
                }
				for (i = 0; i < schema.length; i++) {
					if (datum && datum.hasOwnProperty(schema[i])) {
						this.writeLong(i);
						this.writeDatum(schema[i], datum[schema[i]]);
						return;
					}
				}
				throw "Invalid value " + datum + " for union: " + schema;

			default:
				if (this.schemas[type] === undefined) {
					throw "Unsupported schema type " + type;
				}

				this.writeDatum(this.schemas[type], datum);
		}
	}
}

export interface Schema {
	fullName: string;
	name: string;
	namespace: string;
}

export class SchemaCache {
	//schemas: Object = {};
	constructor(schemaArray: any[]) {
		for (var i = 0; i < schemaArray.length; i++) {
			this.store(schemaArray[i]);
		}
	}

	importSchemas(filePath: string) {

	}

	store(schema) {
		var fullName = schema.namespace ? (schema.namespace + "." + schema.name) : schema.name;
		if (fullName !== undefined) {
			this[fullName] = schema;

			if (schema.fields !== undefined)
				for (var i = 0; i < schema.fields.length; i++) {
					this.store(schema.fields[i].type);
				}
		}
	}

	validate(schema, datum) {
		var type;
		
		var result;

		function typeOf(value) {
			var s:any = typeof value;
			if (s === 'object') {
				if (value) {
					if (value instanceof Array) {
						s = 'array';
					}
				} else {
					s = 'null';
				}
			}
			return s;
		} // typeOf

		type = typeOf(schema);
		if (type === "object") {
			type = schema.type;
		}
		else if (type === "string") {
			type = schema;
		}
		else if (type === "array") {
			type = "union";
		}
		else if (type === "undefined") {
			throw "W:Undefined schema type " + schema;
		}
		else {
			throw "W:Unrecognized schema type: " + type + schema;
		}

		switch (type) {
			// Primitive types
			case "null":
				return T(ty.null)(datum);
			case "boolean":
				T(ty.bool)(datum);
				return true;
			case "int":
			case "long":
				T(ty.int)(datum);
				break;
			case "float":
			case "double":
				T(ty.num.not.nan)(datum);
				break;
			case "bytes":
			case "string":
				T(ty.str)(datum);
				return;
			// Complex types
			case "record":
				for (var i = 0; i < schema.fields.length; i++) {
					this.validate(schema.fields[i].type, datum[schema.fields[i].name]);
				}
				return;

			case "enum":
				for (var i = 0; i < schema.symbols.length; i++) {
					if (schema.symbols[i] == datum) {
						return;
					}
				}
				throw "Invalid enum value: " + datum + " expecting: " + schema.symbols;

			case "array":
				if (datum.length > 0) {
					for (var i = 0; i < datum.length; i++) {
						this.validate(schema.items, datum[i]);
					}
				}
				return;

			case "map":
				var count = 0;
				for (var thisVar in datum) {
					if (datum.hasOwnProperty(thisVar)) {
						++count;
					}
				}

				if (count > 0) {
					for (var k in datum) {
					}
				}
				break;

			case "union":
				for (var i = 0; i < schema.length; i++) {
					if (datum==null && schema[i] === 'null') {
						return;
					}
					if (datum && datum[schema[i]] != undefined) {
						return;
					}
				}
				throw "Invalid value " + datum + " for union: " + schema;

			default:
				if (this[type] === undefined) {
					throw "Unsupported schema type " + type;
				}

				this.validate(this[type], datum);
		}
	}
}